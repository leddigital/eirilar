<?php

return [
    'raiz-title' => 'Departamento de Exportaciones',
    'title' => 'Sea un distribuidor',
    'text' => '
    <p>
    Una de las mayores industrias del segmento en Brasil, Eirilar cuenta con un complejo industrial con cerca de 93.000 metros cuadrados. La unidad está ubicada a orillas de la carretera Rodovia Euclides da Cunha, Km 474,5, en Tanabi, estado de São Paulo, y es responsable de la producción, desarrollo y logística de sus productos.
    </p>
    <p>
    Fundada en 1.977, la industria de aluminio Eirilar trabaja constantemente en la investigación de nuevas tecnologías incorporadas al proceso productivo, principalmente para que sean sistemáticamente más eficientes y ecológicas.
    </p>
    <h5>Actualmente tenemos más de 15 mil puntos de ventas en Brasil y estamos expandiendo nuestros negocios hacia el mercado internacional.</h5>
    <p>
    Los distribuidores y revendedores de los productos Eirilar podrán contar:
        <ul>
            <li>Línea diversificada de productos ;</li>
            <li>Soporte de marketing para potencializar las ventas;</li>
            <li>Contratos exclusivos con el local de destino de la comercialización;</li>
            <li>Soporte especializado para el comércio exterior.</li>
        </ul>
    </p>
    <p></p>
    <p>
        <strong>Teléfono:</strong> 0800 707 9220<br />
        <strong>Email:</strong> <a href="mailto:vendas@eirilar.com.br">vendas@eirilar.com.br</a>
    </p>
    ',
    'company' => 'Company',
    'subject' => 'Subject',
    'message' => 'Message',
    'send' => 'Send'
];


