<?php

return [
    'button' => 'leer más',
    'material' => 'Material',
    'espessura' => 'Espesor',
    'rev_interno' => 'Forro Interior',
    'rev_externo' => 'Recubrimiento Externo',
    'tampa' => 'Tapa',
    'vaporizador' => 'Vaporizador',
    'pomeis' => 'Granadas',
    'alca' => 'Correa',
    'agulheto' => 'Boquilla',
    'cover' => 'Tampa',
    'holder' => 'Cabo',
    'no' => 'No',
    'yes' => 'Sí',
    'search' => 'Buscar',
    'no-search-title' => 'Lo sentimos, no pudimos encontrar coincidencias para su búsqueda.',
    'no-search-sub' => 'Probablemente su búsqueda fue demasiado específica, intente buscar con otro término.'
];
