<?php

return [
    'home'=>'Inicio',
    'search' => 'Buscar',
    'products' => 'Productos',
    'distribution' => 'Conviértete en un Distribuidor',
    'contact' => 'Contacto'
];
