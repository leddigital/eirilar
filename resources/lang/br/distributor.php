<?php

return [
    'title' => 'Seja um distribuidor',
    'text' => '
    <p>
    A Pandin tem 60 anos de tradição e experiência na fabricação de móveis de aço. Consolidada no segmento moveleiro, a marca oferece produtos ergonômicos, funcionais e resistentes com design arrojado.
    </p>
    <p>
    Expertise de mercado que você pode ter em sua loja:
        <ul>
            <li>Linha diversificada de produtos;</li>
            <li>Suporte de marketing para potencializar as vendas;</li>
            <li>Contratos exclusivos com o local de destino da comercialização;</li>
            <li>Suporte especializado ao comércio exterior.</li>
        </ul>
    </p>
    <p></p>
    <p>
        <strong>Telefone:</strong> +55 17 2136 8300<br />
        <strong>E-mail:</strong> <a href="mailto:contato@pandinn.com.br">contato@pandin.com.br</a>
    </p>
    ',
    'company' => 'Company',
    'subject' => 'Subject',
    'message' => 'Message',
    'send' => 'Send'
];
