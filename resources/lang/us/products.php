<?php


return [
    'button' => 'Read More',
    'material' => 'Material',
    'espessura' => 'Thickness',
    'rev_interno' => 'Internal Coating',
    'rev_externo' => 'External Coating',
    'tampa' => 'Lid',
    'vaporizador' => 'Vaporizer',
    'pomeis' => 'Pommel',
    'alca' => 'Handle',
    'agulheto' => 'Nozzle',
    'cover' => 'Lid',
    'holder' => 'Holder',
    'no' => 'No',
    'yes' => 'Yes',
    'search' => 'Search for',
    'no-search-title' => 'Sorry we could not find any matches for your search.',
    'no-search-sub' => 'Maybe your search was too specific, please try searching with another term.'
];
