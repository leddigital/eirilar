@extends('layouts.default')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a
                            href="{{ route('home',['locale'=>Config::get('app.locale')]) }}">Home</a></li>
                    <li class="breadcrumb-item active">
                        <a href="{{ route('subcategories_products',['locale'=>Config::get('app.locale'),'url'=>$categoria->url]) }}">{{ $categoria->nome }}</a>
                    </li>
                </ol>
            </nav>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 text-center">
            <h2>{{ $categoria->nome }}</h2>
        </div>
    </div>
</div>
<section class="products">
    <div class="container">
        <div class="row">
            @foreach ($items as $item)
            <div class="col-md-3 product-item">
                <a href="{{ route('produto',['locale'=>Config::get('app.locale'),'url'=>$item->url]) }}"><img src="{{ asset('img/produtos/'.$item->image) }}" alt="" class="img-fluid"></a>
                <div class="product-name">{{ $item->name }}</div>
                <a class="text-link" href="{{ route('produto',['locale'=>Config::get('app.locale'),'url'=>$item->url]) }}">
                        @lang('products.button')
                </a>
            </div>
            @endforeach
        </div>
        <div class="text-xs-center text-center">
            {{ $items->links() }}
        </div>
    </div>
</section>
@endsection
