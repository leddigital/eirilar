@extends('layouts.default')
@section('content')
<div class="container">
        <div class="row">
                <div class="col-sm-12">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a
                                    href="{{ route('home',['locale'=>Config::get('app.locale')]) }}">Home</a></li>
                            <li class="breadcrumb-item active">
                                @lang('products.search') "<?php echo $_GET['s']; ?>"
                            </li>
                        </ol>
                    </nav>
                </div>
            </div>
    <div class="row">
        <div class="col-sm-12 text-center">
            <h2>@lang('products.search')  "<?php echo $_GET['s']; ?>"</h2>
        </div>
    </div>
</div>

<section class="products">
    <div class="container">
        <div class="row">
            @foreach ($products as $product)
            <div class="col-md-3 product-item">
                <img src="{{ asset('img/produtos/'.$product->image) }}" alt="" class="img-fluid">
                <div class="product-name">{{ $product->name }}</div>
                <a class="text-link" href="{{ route('produto',['locale'=>Config::get('app.locale'),'url'=>$product->url]) }}">
                    @lang('products.button')
            </a>
            </div>
            @endforeach
        </div>
        {{ $paginate->appends(['s' => $_GET['s']])->links() }}
    </div>
</section>
@endsection


