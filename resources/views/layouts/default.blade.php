<!doctype html>
<html lang="{{  Config::get('app.locale')=='es'?'es-419':'en'  }}">

<head>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    @if(!isset($_subtitle))
    <title>Eirilar</title>
    @else
    <title>{{ $_subtitle }} | Eirilar</title>
    @endif

    <link href="https://fonts.googleapis.com/css?family=PT+Sans&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"
        integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <link rel="icon" type="image/ico" href="{{ asset('img/favicon.png') }}">

    <script src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>
    <link rel="stylesheet" href="{{ asset("plugins/sweetalert2/sweetalert2.min.css") }}" />

    <!-- CSS PADRÃO -->
    <link href="{{ asset('css/bootstrap-reboot.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/bootstrap-grid.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/styles.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/responsive.css') }}" rel="stylesheet" type="text/css" />
    @yield('css')
    <!-- JavaScript Padrão -->
    <script src="{{ asset('js/jquery-3.4.1.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    @yield('scripts')

</head>

<body>
    @include('block.header')
    @yield('content')
    @include('block.footer')
</body>

</html>
