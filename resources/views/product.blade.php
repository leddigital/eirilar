@extends('layouts.default')
@section('scripts')
<script>
    var image_change;
    $(function(){
        $('ul.variations li a').on('click',function(e){
            e.preventDefault();
            image_change = $(this).data('image');
            $('#product-image-change').attr('src',image_change);
        });
    });
</script>
@endsection

@section('content')

<section class="products">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a
                                href="{{ route('home',['locale'=>Config::get('app.locale')]) }}">Home</a></li>
                        <li class="breadcrumb-item ">
                            <a href="{{ route('subcategories_products',['locale'=>Config::get('app.locale'),'url'=>$categoria->url]) }}">{{ $categoria->nome }}</a>
                        <li class="breadcrumb-item active" aria-current="page">{{ $product->name }}</li>
                        </li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-4">
                <img id="product-image-change" src="{{ asset('img/produtos/'.$product->image) }}" class="img-fluid">
            </div>
            <div class="col-sm-12 col-md-12 col-lg-8">
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="product-title">{{ $product->name }}</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-5">

                        @foreach ($descriptions as $item)
                        @if ($item['valor'] != '0' && $item['valor'] != '')
                            <div><strong>@lang($item['nome']):</strong> {{ $item['valor'] }}</div>
                        @endif
                        @endforeach

                        <br />
                        {{-- <strong>Descrição</strong>
                        <ul>
                            @if ($product->conf1!="")
                            <li>{{ $product->conf1 }}</li>
                            @endif
                            @if ($product->conf2!="")
                            <li>{{ $product->conf2 }}</li>
                            @endif
                            @if ($product->conf3!="")
                            <li>{{ $product->conf3 }}</li>
                            @endif
                            @if ($product->conf4!="")
                            <li>{{ $product->conf4 }}</li>
                            @endif
                            @if ($product->conf5!="")
                            <li>{{ $product->conf5 }}</li>
                            @endif
                        </ul> --}}
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-7 d-none d-sm-block">
                        <ul class="list-unstyled variation-list">
                            @foreach ($variations as $variation)
                                <li><span>{{ $variation->sku }}</span>  - {{ $variation->nome }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>

@endsection
