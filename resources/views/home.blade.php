@extends('layouts.default')
@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset("plugins/Nivo-Slider-jQuery-master/nivo-slider.css") }}" title="bootstrap">
@endsection
@section('scripts')
<script src="{{ asset("plugins/Nivo-Slider-jQuery-master/jquery.nivo.slider.js") }}"></script>

@endsection
@section('content')
<section class="products-slider d-none d-sm-block">
    <div class="container-fluid">
        <div class="row">

            <div class="slider-wrapper theme-default">
                <div id="slider" class="nivoSlider">
                    @foreach ($banners as $item)
                        <img src="{{ asset('img/banners/'.$item->banner) }}" data-thumb="{{ asset('img/banners/'.$item->banner) }}" alt="" />
                    @endforeach
                </div>
            </div>

        </div>
    </div>
</section>
<section class="linha">
    <div class="container">
        <div class="row">
            @foreach ($line as $linhas)
            <div class="col-md-4 col-sm-6 col-12">
                <a href="{{ route('subcategories_products',['locale'=>Config::get('app.locale'),'url'=>$linhas->url])}}">
                    <div class="block-linha line-wrapper position-relative" style="background-image: url({{ asset('img/linhas/'.$linhas->img) }});"></div>
                </a>
            </div>
            @endforeach
        </div>
    </div>
</section>

<section class="categories">
    <a id="produtos"></a>
    <div class="container">
        <div class="row">
            @foreach ($category as $subcategory)
            <div class="col-md-4 col-6">
                <a href="{{ route('subcategories_products',['locale'=>Config::get('app.locale'),'url'=>$subcategory->url])}}">
                    <div class="product-wrapper position-relative" style="background-image: url({{ asset('img/back-categorias.png') }})">
                        <p>{{ $subcategory->nome }}</p>
                    </div>
                </a>
            </div>
            @endforeach
        </div>
    </div>
</section>



<script type="text/javascript">
    $(window).on('load', function() {
        $('#slider').nivoSlider({
            directionNav: true,
            controlNav: false,
            directionNav: true,
            pauseOnHover: true,
            animSpeed: 500,
            pauseTime: 3000,
            prevText: '<h1 style="font-size: 30px; padding-left: 30px;"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></h1>',
            nextText: '<h1 style="font-size: 30px; padding-right: 30px;"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></h1>',
        });
    });
    </script>

@endsection
