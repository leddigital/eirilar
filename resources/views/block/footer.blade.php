<footer>
    <div class="container">
        <div class="obaservations"></div>
        <div class="copyright">
            &copy; Eirilar Export, 2019. Developed by <a href="http://agencialed.com.br" target="_blank" rel="noopener">Led Digital</a>
        </div>
    </div>
</footer>
{{-- @if (Agent::isMobile())
<a class="whatsbox-button-transparent" href="whatsapp://send?phone=&text=" style="left: 10px; bottom: 10px;">
    <img class="whatsbox-icon" src="{{ asset('img/whatsapp.png') }}">
</a>
@else
<a class="whatsbox-button whatsbox-pulse-button" href="https://web.whatsapp.com/send/?phone=&text="
    style="right: 30px; bottom: 20px;" target="_blank">
    <img class="whatsbox-icon" src="{{ asset('img/whatsapp.png') }}">Chame no WhatsApp agora!
</a>
@endif --}}
