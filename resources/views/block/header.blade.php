<header>
    <div class="container-fluid" style="background-image: url({{ asset('img/bg-header.jpg') }})">
        <div class="row h-100">
            <div class="col-sm-12">
                <a href="{{ route('home',['locale'=>Config::get('app.locale')]) }}">
                    <img class="d-block mx-auto" src="{{ asset('img/logo.png') }}" alt="">
                </a>
            </div>
        </div>
    </div>
</header>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="container">
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link {{ Route::is('home')?'active':'' }}"
                        href="{{ route('home',['locale'=>Config::get('app.locale')]) }}">@lang('site.home')</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ Route::is('homes')?'active':'' }}"
                        href="{{ route('home',['locale'=>Config::get('app.locale')]) }}#produtos">@lang('site.products')</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ Route::is('distribuidor')?'active':'' }}"
                        href="{{ route('distribuidor',['locale'=>Config::get('app.locale')]) }}">@lang('site.distribution')</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link {{ Route::is('distribuidor-contato')?'active':'' }}"
                        href="{{ route('distribuidor-contato',['locale'=>Config::get('app.locale')]) }}">@lang('site.contact')</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      {{  Config::get('app.locale')=='es'?'Español':'English'  }}
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                      <a class="dropdown-item" href="{{ route('lang',['lang'=>'es'] )}}">
                        <img src="{{ asset('img/es.png') }}" class="img-fluid {{ Config::get('app.locale')=='es'?'selected':'' }}" /> Español
                      </a>
                      <a class="dropdown-item" href="{{ route('lang',['lang'=>'us'] )}}">
                        <img src="{{ asset('img/us.png') }}" class="img-fluid {{ Config::get('app.locale')=='us'?'selected':'' }}" /> English
                      </a>
                    </div>
                  </li>
            </ul>
            <form action="{{route('pesquisar',['locale'=>Config::get('app.locale')])}}" id="search-form" method="get"
                class="form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-2" name="s" type="search" placeholder="@lang('site.search')..."
                    aria-label="Search">
                <button class="btn btn-default my-2 my-sm-0" type="submit">@lang('site.search')</button>
            </form>
        </div>
    </div>
</nav>
