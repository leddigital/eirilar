<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clicks extends Model
{
    protected $table = "clicks";
    //protected $fillable = [];
    protected $guarded = [];
    public $timestamps = false;
}
