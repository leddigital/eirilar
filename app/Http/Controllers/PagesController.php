<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categories;
use App\Variations;
use App\SubCategories;
use App\Products;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;
use App\Banners;
use App\Clicks;

class PagesController extends Controller
{
    public $lang;
    protected $model_clicks;

    public function __construct()
    { 
        $this->model_clicks = new Clicks();
    }

    public function home()
    {
        $lang = \Lang::getLocale();

        //Seleciona manualmente a categoria principal
        $category = Categories::where('lang', '=', $lang)->where('principal', '<>', '1')->get();
        $line = Categories::where('principal', '=', '1')->where('lang', '=', $lang)->get();


        //Selecionar todos os banner

        $banners = Banners::where('lang', '=', $lang)->get();

        return view('home', ['banners' => $banners, 'category' => $category, 'line' => $line]);
    }

    //Listagem de produtos da subcategorias
    public function subcategories_products(Request $req)
    {
        $url = $req->url;

        $categoria = Categories::where('url', '=', $url)->get()->first();
        $items = $categoria->products()->paginate(12);

        return view('subcategories_products', [
            'categoria' => $categoria,
            'items' => $items
        ]);
    }

    public function product(Request $request)
    {
        $url = $request->url;
        $product = Products::where('url', '=', $url)->get()->first();
        $categoria = $product->categories()->get()->first();
        $item = $product->categories()->get()->first();
        $variations = $product->variations()->get();

        $data[] = array(
            "nome" => "products.material",
            "valor" => $product->material
        );

        $data[] = array(
            "nome" => "products.espessura",
            "valor" => $product->espessura
        );

        $data[] = array(
            "nome" => "products.rev_interno",
            "valor" => $product->rev_interno
        );

        $data[] = array(
            "nome" => "products.rev_externo",
            "valor" => $product->rev_externo
        );

        $data[] = array(
            "nome" => "products.tampa",
            "valor" => $product->tampa
        );

        $data[] = array(
            "nome" => "products.vaporizador",
            "valor" => $product->vaporizador
        );

        $data[] = array(
            "nome" => "products.holder",
            "valor" => $product->holder
        );

        $data[] = array(
            "nome" => "products.pomeis",
            "valor" => $product->pomeis
        );

        $data[] = array(
            "nome" => "products.alca",
            "valor" => $product->alca
        );

        $data[] = array(
            "nome" => "products.agulheto",
            "valor" => $product->agulheto
        );

        //Pegar informações adicionais do client
        $a = $this->call_api('get', 'http://api.ipapi.com/' . $this->get_client_ip(), ['access_key' => 'b67f5b2e5cd847ddd0aaa8df00912e52']);
        $locale = json_decode($a);

        $clicks = [
            'product_id' => $product->id,
            'ip' => $locale->ip,
            'country' => $locale->country_name,
            'region' => $locale->region_name,
            'city' => $locale->city,
            'lat' => $locale->latitude,
            'lng' => $locale->longitude,
            'url' => $url,
            'from' => 'site'
        ];

        //Inserir no banco de dados
        $this->model_clicks->create($clicks);

        return view('product', [
            'product' => $product,
            'variations' => $variations,
            'categoria' => $categoria,
            'descriptions' => $data,
            '_subtitle' => $product->name
        ]);
    }

    public function search(Request $req)
    {
        $lang = \Lang::getLocale();
        $term = $req->query('s');

        $products = Products::whereHas('Variations',function($q) use ($term,$lang){
            $q->where('lang', '=', $lang)
            ->where(function ($query) use ($term) {
                $query
                    ->orWhere('sku','=',$term);
            });
        })->orWhere('name', 'like', '%' . $term . '%');


        return view('search', [
            'products' => $products->get(),
            'paginate'=>$products->paginate(12)
        ]);
    }

    public function email(Request $req)
    {
        $form = $req->all();
        Mail::to('digital@agencialed.com.br')
            ->send(new SendMail($form));

        if (Mail::failures()) {
            echo '0';
            exit;
        }
        echo '1';
    }
}
