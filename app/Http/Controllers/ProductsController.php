<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SubCategories;
use App\Products;
use App\Variations;
use Illuminate\Support\Facades\Storage;
use App\Categories;

class ProductsController extends Controller
{
    //Importar as categorias
    public function produtos() {

        $model_products = new Products();

        $handle = fopen('products-english.csv', 'r');
        $header = true;

        //$last_insert = null;
        $last_id = 1;

        echo "<pre>";
        while (($data = fgetcsv($handle, 10000, "	"))) {

            if ($header) {
                $header = false;
            } else {
                if ($data[1] == "000001") {

                    $produto = [
                        'lang' => $data[0],
                        'name' => $data[3],
                        'image' => $data[2],
                        'sku' => $data[1],
                        'url' => $this->url_verify($data[3],  $model_products),
                        'espessura' => $data[6],
                        'material' => $data[7],
                        'rev_interno' => $data[8],
                        'rev_externo' => $data[9],
                        'tampa' => $data[10],
                        'vaporizador' => $data[11],
                        'holder' => $data[12],
                        'pomeis' => $data[13],
                        'alca'   => $data[14],
                        'agulheto' => $data[15]
                    ];

                    //print_r($produto);

                    $inserted_product = Products::create($produto);

                    $inserted_product->categories()->attach($data[4]);
                    $inserted_product->categories()->attach($data[5]);
                    // echo $insert_id = $inserted_product->id;

                    if ($inserted_product) {
                        echo "Cadastrado com sucesso";
                    } else {
                        echo "Erro ao cadastrar";
                    }

                } else {

                    $url = $this->url_verify($data[3],  $model_products);
                    $last_id = $inserted_product->id;


                    // if ($data[2] != $last_insert) {
                    //     $last_id++;
                    // }

                    $variation = [
                        'nome' => $data[3],
                        'image' => $data[2],
                        'sku' => $data[1],
                        'products_id' => $last_id,
                        'espessura' => $data[6],
                        'material' => $data[7],
                        'rev_interno' => $data[8],
                        'rev_externo' => $data[9],
                        'tampa' => $data[10],
                        'vaporizador' => $data[11],
                        'holder' => $data[12],
                        'pomeis' => $data[13],
                        'alca' => $data[14],
                        'agulheto' => $data[15],
                        'lang' => $data[0]
                    ];

                    //print_r($variation);

                    //$last_insert = $data[2];

                    $inserted_variation = Variations::create($variation);
                    if ($inserted_variation) {
                        echo "Variação " . $data[3] . " cadastrada com sucesso! <br>";
                    } else {
                        echo "Erro ao cadastrar variação <br>";
                    }
                }
            }
        }

        echo "</pre>";
        fclose($handle);
    }

    public function addcategoria(){

        $handle = fopen('categorias.csv', 'r');
        $header = true;

        echo "<pre>";
        while ($data = fgetcsv($handle, 10000, ";")) {

            $model_products = new Categories();

            if ($header) {
                $header = false;
            } else {

                $categoria = [
                    'id' => $data[0],
                    'nome' => $data[1],
                    'principal' => $data[2],
                    'lang' => $data[3],
                    'url' => $this->url_verify($data[1], $model_products)
                ];

                $inserted_category = Categories::create($categoria);
            }

            if ($inserted_category) {
                echo "Categorias adicionadas com sucesso!";
            } else {
                echo "Falha ao adicionar categorias";
            }

        }

    }




    //Verificação de URL
    protected function url_verify($string, $model, $id = '')
    {
        $slugfy = $this->slugify($string, '-');

        //Caso não tenha passado o id
        if ($id == "") {
            $url_result_verify = $model->where('url', $slugfy)->get()->first();
            if (isset($url_result_verify)) {
                $return = false;
                $i = 1;
                do {
                    $url_new = $slugfy . '-' . $i++;
                    $url_result_verify = $model->where('url', $url_new)->first();
                    if (!isset($url_result_verify->url)) {
                        $return = true;
                    }
                } while ($return == false);
                return strtolower($url_new);
                exit;
            } else {
                return strtolower($slugfy);
                exit;
            }
        } else {
            $url_result_verify = $model
                ->where('url', $slugfy)
                ->where('id', '<>', $id)
                ->first();

            if (isset($url_result_verify)) {

                $return = false;
                $i = 1;
                do {
                    $url_new = $slugfy . '-' . $i++;
                    $url_result_verify = $model
                        ->where('url', $url_new)
                        ->where('id', '<>', $id)
                        ->first();
                    if ($url_result_verify->url == "") {
                        $return = true;
                    }
                } while ($return == false);
                return strtolower($url_new);
                exit;
            } else {
                return strtolower($slugfy);
                exit;
            }
        }
    }

    //Remover hifens e espaços
    public function clean($string)
    {
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
        $string = str_replace('-', '', $string); // Replaces all spaces with hyphens.
        return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }

    //URL Amigável
    public function slugify($text)
    {
        // $text = preg_replace('~[^\pL\d]+~u', '-', $text);
        // $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
        // $text = preg_replace('~[^-\w]+~', '', $text);
        // $text = trim($text, '-');
        // $text = preg_replace('~-+~', '-', $text);
        // $text = strtolower($text);
        // if (empty($text)) {
        //     return 'n-a';
        // }
        // return $text;

        $string = preg_replace('/[\t\n]/', ' ', $text);
        $string = preg_replace('/\s{2,}/', ' ', $string);
        $list = array(
            'Š' => 'S',
            'š' => 's',
            'Đ' => 'Dj',
            'đ' => 'dj',
            'Ž' => 'Z',
            'ž' => 'z',
            'Č' => 'C',
            'č' => 'c',
            'Ć' => 'C',
            'ć' => 'c',
            'À' => 'A',
            'Á' => 'A',
            'Â' => 'A',
            'Ã' => 'A',
            'Ä' => 'A',
            'Å' => 'A',
            'Æ' => 'A',
            'Ç' => 'C',
            'È' => 'E',
            'É' => 'E',
            'Ê' => 'E',
            'Ë' => 'E',
            'Ì' => 'I',
            'Í' => 'I',
            'Î' => 'I',
            'Ï' => 'I',
            'Ñ' => 'N',
            'Ò' => 'O',
            'Ó' => 'O',
            'Ô' => 'O',
            'Õ' => 'O',
            'Ö' => 'O',
            'Ø' => 'O',
            'Ù' => 'U',
            'Ú' => 'U',
            'Û' => 'U',
            'Ü' => 'U',
            'Ý' => 'Y',
            'Þ' => 'B',
            'ß' => 'Ss',
            'à' => 'a',
            'á' => 'a',
            'â' => 'a',
            'ã' => 'a',
            'ä' => 'a',
            'å' => 'a',
            'æ' => 'a',
            'ç' => 'c',
            'è' => 'e',
            'é' => 'e',
            'ê' => 'e',
            'ë' => 'e',
            'ì' => 'i',
            'í' => 'i',
            'î' => 'i',
            'ï' => 'i',
            'ð' => 'o',
            'ñ' => 'n',
            'ò' => 'o',
            'ó' => 'o',
            'ô' => 'o',
            'õ' => 'o',
            'ö' => 'o',
            'ø' => 'o',
            'ù' => 'u',
            'ú' => 'u',
            'û' => 'u',
            'ý' => 'y',
            'ý' => 'y',
            'þ' => 'b',
            'ÿ' => 'y',
            'Ŕ' => 'R',
            'ŕ' => 'r',
            '/' => '-',
            ' ' => '-',
            '.' => '-',
        );

        $string = strtr($string, $list);
        $string = preg_replace('/-{2,}/', '-', $string);
        $string = strtolower($string);

        return $string;
    }

    public function filter_products() {
        $handle = fopen('filtro-produtos.csv', 'r');

        while (($n = fgetcsv($handle, 10000, ";"))) {
            $data[] = $n[0];
        }

        $directory = '/home/edinaldo/projetos/eirilar/public/img/produtos/';
        $files = scandir($directory);
        opendir($directory);

        foreach ($data as $value) {
            if (in_array($value, $files)) {

                foreach ($files as $item) {
                    if ($item == $value) {

                        $old = $directory . $value;
                        $new = "/home/edinaldo/projetos/eirilar/public/img/bkp-img/" . $value;

                        $flag = rename($old, $new);
                        if ($flag) {
                            echo "Arquivo " . $value . " movido com sucesso para : " . $new . "<br>";
                        } else {
                            echo "Erro ao enviar arquivo <br>";
                        }
                    }
                }
            }
        }
    }
}
