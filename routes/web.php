<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('setlocale/{lang}', ['as' => 'lang', function ($locale) {
    if (in_array($locale, \Config::get('app.locales'))) {
        session(['locale' => $locale]);
    }
    $lang = \Lang::getLocale();
    return redirect($locale);
}]);

Route::get('/', function () {
    $lang = \Lang::getLocale();
    return redirect($lang);
});

Route::group(['prefix' => '{locale}'], function () {

    Route::get('/', ['as' => 'home', 'uses' => "PagesController@home"]);

    Route::get('/subcategory/{url}', ['as' => 'subcategories_products', 'uses' => "PagesController@subcategories_products"]);

    Route::get('/product/{url}', ['as' => 'produto', 'uses' => "PagesController@product"]);

    Route::get('/search', ['as' => 'pesquisar', 'uses' => "PagesController@search"]);

    #Route::get('/produtos',['as'=>'distribuidor','uses'=>'ProductsController@produtos']);

    Route::get('/distributor', ['as' => 'distribuidor', 'uses' => function () {

        return view('distribuidor', [
            '_subtitle' => trans('site.distribution')
        ]);
    }]);

    Route::get('/distributor#contact', ['as' => 'distribuidor-contato', 'uses' => function () {
        return view('distribuidor-contato', [
            '_subtitle' => "Seja um Distribuidor"
        ]);
    }]);

    Route::post('/enviar/email', ['as' => 'sendmail', 'uses' => 'PagesController@email']);
});
